﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public enum GameState
    {
        NotStarted,
        Started,
        GameOver,
        Win
    }

    public GameState _gameState;

    public GameObject player;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        //Application.targetFrameRate = 60;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
            Debug.Break();
    }
}