﻿using ElephantSDK;
using GameAnalyticsSDK;
using MoreMountains.NiceVibrations;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    [SerializeField] private GameObject levelsParent;

    [HideInInspector] public int currentLevel;

    //[HideInInspector] public Vector3 currentLevelFinishArea;
    //[HideInInspector] public float currentLevelLength;

    [BoxGroup("Level Test Settings")] public bool testMode;
    [BoxGroup("Level Test Settings")] public int testLevel;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        if (!testMode)
            currentLevel = PlayerPrefs.GetInt("currentLevel", 0);
        else
            currentLevel = testLevel - 1;

        for (int i = 0; i < levelsParent.transform.childCount; i++)
            levelsParent.transform.GetChild(i).gameObject.SetActive(false);

        levelsParent.transform.GetChild(currentLevel).gameObject.SetActive(true);
    }

    private void Start()
    {
        UIManager.Instance.currentLevelText.text = (currentLevel + 1).ToString();
        UIManager.Instance.nextLevelText.text = (currentLevel + 2).ToString();

        //currentLevelFinishArea = ;
        //currentLevelLength = Vector3.Distance(GameManager.Instance.player.transform.position, currentLevelFinishArea);
    }

    private void LateUpdate()
    {
        //var ratio = (currentLevelLength - Vector3.Distance(GameManager.Instance.player.transform.position, currentLevelFinishArea)) / (currentLevelLength);
        //UIManager.Instance.UpdateProgressBar(ratio);
    }

    public void GameOver()
    {
        GameManager.Instance._gameState = GameManager.GameState.GameOver;

        MMVibrationManager.Haptic(HapticTypes.Failure);

        Elephant.LevelFailed(currentLevel);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, currentLevel.ToString());
    }

    public void LevelCompleted()
    {
        GameManager.Instance._gameState = GameManager.GameState.Win;

        MMVibrationManager.Haptic(HapticTypes.Success);

        Elephant.LevelCompleted(currentLevel);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, currentLevel.ToString());

        if (currentLevel != levelsParent.transform.childCount - 1)
            currentLevel += 1;
        else
            currentLevel = 0;

        if (!testMode) PlayerPrefs.SetInt("currentLevel", currentLevel);

        UIManager.Instance.OpenWinPanel();
    }
}