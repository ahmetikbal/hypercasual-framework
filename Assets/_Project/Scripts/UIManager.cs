﻿using DG.Tweening;
using ElephantSDK;
using GameAnalyticsSDK;
using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    public GameObject mainMenu;
    public GameObject losePanel;
    public GameObject winPanel;

    public TextMeshProUGUI currentLevelText, nextLevelText;
    public Image levelProgressBar;

    public Image hapticIcon;
    public Sprite hapticOn, hapticOff;
    private int hapticStatus;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        hapticStatus = PlayerPrefs.GetInt("hapticStatus", 1);

        if (hapticStatus == 1)
            hapticIcon.sprite = hapticOn;
        else
            hapticIcon.sprite = hapticOff;
    }

    public void HapticButton()
    {
        if (hapticStatus == 1)
        {
            MMVibrationManager.SetHapticsActive(false);
            hapticIcon.sprite = hapticOff;

            hapticStatus = 0;
            PlayerPrefs.SetInt("hapticStatus", hapticStatus);
        }
        else
        {
            MMVibrationManager.SetHapticsActive(true);
            hapticIcon.sprite = hapticOn;

            hapticStatus = 1;
            PlayerPrefs.SetInt("hapticStatus", hapticStatus);

            MMVibrationManager.Haptic(HapticTypes.Selection);
        }
    }

    public void RestartTheScene()
    {
        MMVibrationManager.Haptic(HapticTypes.Selection);

        DOTween.KillAll();

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void OpenWinPanel()
    {
        winPanel.SetActive(true);
        winPanel.GetComponent<CanvasGroup>().alpha = 0;
        winPanel.GetComponent<CanvasGroup>().DOFade(1f, 1f);
    }

    public void OpenLosePanel()
    {
        losePanel.SetActive(true);
        losePanel.GetComponent<CanvasGroup>().alpha = 0;
        losePanel.GetComponent<CanvasGroup>().DOFade(1f, 1f);
    }

    public void PlayArea()
    {
        mainMenu.SetActive(false);
        GameManager.Instance._gameState = GameManager.GameState.Started;
        MMVibrationManager.Haptic(HapticTypes.Selection);

        Elephant.LevelStarted(LevelManager.Instance.currentLevel);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, LevelManager.Instance.currentLevel.ToString());
    }

    public void UpdateProgressBar(float ratio)
    {
        levelProgressBar.fillAmount = ratio;
    }
}