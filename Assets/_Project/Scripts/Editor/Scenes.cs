﻿using UnityEditor;
using UnityEditor.SceneManagement;

public class Scenes
{
    [MenuItem("SCENES/Elephant Splash",priority = 0)]
    private static void OpenSplashScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/Elephant/elephant_scene.unity", OpenSceneMode.Single);
        }
    }

    [MenuItem("SCENES/Game Scene")]
    private static void OpenGameScene()
    {
        if (!EditorApplication.isPlaying)
        {
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            EditorSceneManager.OpenScene("Assets/_Project/Scenes/Game.unity", OpenSceneMode.Single);
        }
    }
}